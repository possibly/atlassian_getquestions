Confluence.QuickNav = (function($) {
    var dropDownPostProcess = [];
    var makeParams;

    return {
        addDropDownPostProcess: function(dp) {
            if(typeof dp !== "undefined") {
                dropDownPostProcess.push(dp);
            } else {
                AJS.log("WARN: Attempted to add a dropdown post-process function that was undefined.");
            }
        },
        setMakeParams: function(mp) {
            makeParams = mp;
        },
        init : function(quickSearchInputField, dropDownPlacement) {
            quickSearchInputField.quicksearch("/json/contentnamesearch.action", null, {
                dropdownPlacement : dropDownPlacement,
                dropdownPostprocess : function(dd) {
                    $.each(dropDownPostProcess, function(index, postProcessFunction) {
                        postProcessFunction && postProcessFunction(dd);
                    });
                },
                makeParams: function(value) {
                    // if the makeParams function was set use that one instead of the default
                    if (makeParams)
                        return makeParams(value);
                    else
                        return { query : value };
                }
            });
        }
    }
})(AJS.$);

AJS.toInit(function() {
    /**
     * Append the drop down to the form element with the class quick-nav-drop-down
     */
    var quickNavPlacement = function (input) {
        return function (dropDown) {
            input.closest("form").find(".quick-nav-drop-down").append(dropDown);
        };
    };

    /**
     * Add the space name to the dropdown field.
     * @param dd
     */
    var addSpaceName = function(dd) {
        AJS.$("a", dd).each(function () {
            var $a = $(this);
            var $span = $a.find("span");
            // get the hidden space name property from the span
            var spaceName = AJS.dropDown.getAdditionalPropertyValue($span, "spaceName");
            if (spaceName && !$a.is(".content-type-spacedesc")) {
                // clone the original link for now. This could potentially link to the space?
                $a.after($a.clone().attr("class", "space-name").html(spaceName));
                // add another class so we can restyle to make room for the space name
                $a.parent().addClass("with-space-name");
            }

        });
    };

    var quickSearchQuery = AJS.$("#quick-search-query"),
        spaceBlogSearchQuery = AJS.$("#space-blog-quick-search-query"),
        confluenceSpaceKey = AJS.$("#confluence-space-key");

    var placementFunction = quickNavPlacement(quickSearchQuery);

    // Choose and Setup the QuickNav
    var quickNav = AJS.defaultIfUndefined("Atlassian.SearchableApps.QuickNav", { defaultValue: Confluence.QuickNav });
    quickNav.init(quickSearchQuery, placementFunction);
    quickNav.addDropDownPostProcess(addSpaceName);

    if (spaceBlogSearchQuery.length && confluenceSpaceKey.length) {
        spaceBlogSearchQuery.quicksearch("/json/contentnamesearch.action?type=blogpost&spaceKey=" + 
                AJS("i").html(confluenceSpaceKey.attr("content")).text(), null, {
            dropdownPlacement : quickNavPlacement(spaceBlogSearchQuery)
        });
    }
});
