AJS.toInit(function ($) {

    var searchIndexProgress = $("#search-index-task-progress-container"),
        reindexTaskInProgress = $("#reindex-task-in-progress").length > 0,
        buildSearchIndexButton = $("#build-search-index-button"),
        searchIndexExists = $("#search-index-exists").length > 0,
        searchIndexDisablingOverlay = $("#search-index-disabling-overlay"),
        searchOverlayMessage = $("#search-overlay-message"),
        searchIndexPanelContents = $("#search-index-panel-contents"),
        searchIndexElapsedTime = $("#search-index-elapsed-time"),
        searchIndexElapsedTimeContainer = $("#search-index-elapsed-time-container"),
        searchIndexErrorStatus = $("#search-index-error-status"),
        searchIndexSuccessStatus = $("#search-index-success-status"),
        searchIndexInProgressStatus = $("#search-index-inprogress-status");

    var dymIndexProgress = $("#didyoumean-index-task-progress-container"),
        dymIndexInProgress = $("#didyoumean-index-in-progress").length > 0,
        buildDymIndexButton = $("#build-didyoumean-index-button"),
        dymIndexExists = $("#didyoumean-index-exists").length > 0,
        dymIndexDisablingOverlay = $("#didyoumean-index-disabling-overlay"),
        readyToBuildDymIndex = $("#ready-to-build-didyoumean-index").length > 0,
        dymLanguageCorrect = $("#language-correct-for-dym").length > 0,
        dymIndexPanelContents = $("#didyoumean-index-panel-contents"),
        dymOverlayMessage = $("#didyoumean-overlay-message"),
        dymIndexElapsedTime = $("#didyoumean-index-elapsed-time"),
        dymIndexElapsedTimeContainer = $("#didyoumean-index-elapsed-time-container"),
        dymIndexErrorStatus = $("#didyoumean-index-error-status"),
        dymIndexSuccessStatus = $("#didyoumean-index-success-status"),
        dymIndexInProgressStatus = $("#didyoumean-index-inprogress-status");

    searchIndexDisablingOverlay.hide();
    dymIndexDisablingOverlay.hide();

    if (!searchIndexExists || searchIndexElapsedTime.html() == '') {
        searchIndexElapsedTimeContainer.hide();
    }

    if (!dymIndexExists || dymIndexElapsedTime.html() == '') {
        dymIndexElapsedTimeContainer.hide();
    }

    searchIndexProgress.progressBar(0);

    if (reindexTaskInProgress || !readyToBuildDymIndex) {
        // disable dym panel
        dymIndexDisablingOverlay.show();
        dymIndexPanelContents.addClass("faded");
        buildDymIndexButton.prop("disabled", true); // ie overlay doesn't prevent this button from being pressed, so we have to do it manually :/

        if (reindexTaskInProgress) {
            dymOverlayMessage.html($("#i18n-key-search-build-in-progress").val());
        } else {
            var overlayMessageHtml;
            if (!dymLanguageCorrect)
                overlayMessageHtml = $("#i18n-key-didyoumean-wrong-language").val();
            else
                overlayMessageHtml = $("#i18n-key-build-index-first").val();
            
            dymOverlayMessage.html(overlayMessageHtml);
        }
    }

    if (reindexTaskInProgress) {

        buildSearchIndexButton.prop("disabled", true);

        var searchInterval = setInterval(function () {
            $.getJSON(contextPath + '/json/reindextaskprogress.action', function (data) {
                searchIndexProgress.progressBar(data.percentageComplete);

                searchIndexElapsedTimeContainer.show(); 
                searchIndexElapsedTime.html(data.compactElapsedTime);

                if (data.percentageComplete == 100) {
                    buildSearchIndexButton.prop("disabled", false);
                    buildDymIndexButton.prop("disabled", false);

                    // renable dym panel
                    dymIndexDisablingOverlay.hide();
                    dymIndexPanelContents.removeClass("faded");

                    searchIndexSuccessStatus.show();
                    searchIndexErrorStatus.hide();
                    searchIndexInProgressStatus.hide();

                    clearInterval(searchInterval);
                }
            });
        }, 2000);
    }

    if (searchIndexExists && !reindexTaskInProgress) {
        searchIndexProgress.progressBar(100);
    }

    if (reindexTaskInProgress) {
        searchIndexInProgressStatus.show();
        searchIndexErrorStatus.hide();
        searchIndexSuccessStatus.hide();
    } else if (searchIndexExists) {
        searchIndexSuccessStatus.show();
        searchIndexErrorStatus.hide();
        searchIndexInProgressStatus.hide();
    } else {
        searchIndexErrorStatus.show();
        searchIndexSuccessStatus.hide();
        searchIndexInProgressStatus.hide();
    }

    dymIndexProgress.progressBar(0);

    if (dymIndexInProgress) {
        // disable search panel
        searchIndexDisablingOverlay.show();
        searchIndexPanelContents.addClass("faded");
        buildSearchIndexButton.prop("disabled", true); // ie overlay doesn't prevent this button from being pressed, so we have to do it manually :/

        searchOverlayMessage.html($("#i18n-key-didyoumean-build-in-progress").val());
    }

    if (dymIndexInProgress) {
        buildDymIndexButton.attr("disabled", true);
        var dymInterval = setInterval(function () {
            $.getJSON(contextPath + '/admin/didyoumean/index-progress.action', function (data) {
                dymIndexProgress.progressBar(data.percentComplete);

                dymIndexElapsedTimeContainer.show(); 
                dymIndexElapsedTime.html(data.compactElapsedTime);

                if (data.percentComplete == 100) {
                    buildDymIndexButton.prop("disabled", false);
                    buildSearchIndexButton.prop("disabled", false);

                    searchIndexDisablingOverlay.hide();
                    searchIndexPanelContents.removeClass("faded");

                    dymIndexSuccessStatus.show();
                    dymIndexErrorStatus.hide();
                    dymIndexInProgressStatus.hide();

                    clearInterval(dymInterval);
                }
            });
        }, 2000);
    }

    if (dymIndexExists && !dymIndexInProgress) {
        dymIndexProgress.progressBar(100);
    }

    if (dymIndexInProgress) {
        dymIndexInProgressStatus.show();
        dymIndexErrorStatus.hide();
        dymIndexSuccessStatus.hide();
    } else if (dymIndexExists) {
        dymIndexSuccessStatus.show();
        dymIndexErrorStatus.hide();
        dymIndexInProgressStatus.hide();
    } else {
        dymIndexErrorStatus.show();
        dymIndexSuccessStatus.hide();
        dymIndexInProgressStatus.hide();
    }

});

