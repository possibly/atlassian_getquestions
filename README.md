# Summary #

Confluence plugin that uses xhtml-macro module to output a link to the latest Atlassian Answers question.

Completed as a part of a code test by Dennis at Atlassian SF.

### TO DO: ###
* Use a velocity file to contain all html and javascript.
* Stop using xhtml-macro and use macro instead.